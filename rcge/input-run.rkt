#lang racket/base

(require "input-driver.rkt"
         data/queue)

(provide call-with-input-ctx)

(define (call-with-input-ctx proc)
  (parameterize ((current-input-ctx ; must be initialized before everything
                  (input-ctx (make-queue)
                             (make-hash)
                             (make-semaphore 1)
                             (make-semaphore)
                             #t)))
    (proc)))
