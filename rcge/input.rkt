#lang racket/base

(require data/queue
         "input-driver.rkt")

(provide key-pressed?
         key-get!
         key-down?
         key-queue-enable!
         key-queue-flush!)

(define (key-pressed?)
  (define ctx (current-input-ctx))
  (call-with-semaphore
   (input-ctx-key-queue-semaphore ctx)
   (λ ()
     (non-empty-queue? (input-ctx-key-queue ctx)))))

(define (key-get!)
  (define ctx (current-input-ctx))
  (semaphore-wait (input-ctx-key-count-semaphore ctx))
  (dequeue! (input-ctx-key-queue ctx)))

(define (key-down? keycode)
  (define ctx (current-input-ctx))
  (hash-ref (input-ctx-key-states ctx) keycode #f))

(define (key-queue-enable! (enable #t))
  (define ctx (current-input-ctx))
  (set-input-ctx-key-queue-enabled! ctx enable))

(define (key-queue-flush!)
  (define ctx (current-input-ctx))
  (define key-queue (input-ctx-key-queue ctx))
  (define key-count-semaphore (input-ctx-key-count-semaphore ctx))
  (call-with-semaphore
   (input-ctx-key-queue-semaphore ctx)
   (λ ()
     (queue-filter! key-queue (λ (x) #f))
     (let loop ()
       (when (semaphore-try-wait? key-count-semaphore)
         (loop))))))
