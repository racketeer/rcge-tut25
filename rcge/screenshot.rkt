#lang racket/base

(provide take-screenshot)

(require "draw.rkt"
         racket/draw/unsafe/cairo
         "blit.rkt"
         (only-in racket/draw make-bitmap bitmap%)
         racket/class)

(define (take-screenshot filename (kind #f))
  (define-values (width height framebuffer) (current-width-height-framebuffer))
  (define bitmap (make-bitmap width height))
  (define handle (send bitmap get-handle))
  (define data (cairo_image_surface_get_data* handle))
  (define stride (cairo_image_surface_get_stride handle))
  (case (system-type 'vm)
    ((racket) (blit-to-cairo-data*-u16vector-hack framebuffer width height data stride))
    ((chez-scheme) (blit-to-cairo-data*-ptr-set-u8 framebuffer width height data stride)))
  (when (not kind)
    (set! kind
          (cond ((regexp-match #rx"\\.bmp$" filename)
                 'bmp)
                ((regexp-match #rx"\\.jpe*g$" filename)
                 'jpeg)
                (else
                 ; defaults to png
                 'png))))
  (send bitmap save-file filename kind))

