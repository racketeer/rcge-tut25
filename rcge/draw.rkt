#lang racket/base

(provide current-draw-ctx
         (struct-out draw-ctx)
         current-width-height-framebuffer
         current-width-height
         call-with-draw-ctx)

(require "driver-run.rkt"
         racket/fixnum)

(define current-draw-ctx (make-parameter #f))

(struct draw-ctx
  (
   (width #:mutable)
   (height #:mutable)
   (framebuffer #:mutable)
   size-callbacks
   ))

(define (current-width-height-framebuffer)
  (define ctx (current-draw-ctx))
  (values (draw-ctx-width ctx)
          (draw-ctx-height ctx)
          (draw-ctx-framebuffer ctx)))

(define (current-width-height)
  (define current (current-draw-ctx))
  (values (draw-ctx-width current)
          (draw-ctx-height current)))

(define (call-with-draw-ctx proc)
  (define driver (current-rcge-driver))
  (define-values (init-width init-height) (apply values ((rcge-driver-get-size driver))))
  (parameterize ((current-draw-ctx
                  (draw-ctx init-width
                            init-height
                            (make-fxvector (fx* init-width init-height))
                            (make-hash))))
    (proc)))
