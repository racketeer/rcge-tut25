#lang racket/base

(provide call-with-render-ctx
         rcge-3d-add-triangle
         make-pipeline-vertex
         rcge-3d-camera
         rcge-3d-projection
         rcge-3d-show-borders!

         scene-render

         rcge-add-light-source

         render-keep-ccw
         render-keep-cw)

(require (for-syntax racket/base
                     racket/list
                     racket/format)
         (only-in racket/fixnum make-fxvector)
         (only-in racket/flonum make-flvector)
         "draw.rkt"
         "algebra.rkt"
         racket/require
         (filtered-in
          (λ (name)
            (regexp-replace #rx"unsafe-" name ""))
          racket/unsafe/ops)
         (only-in
          (rename-in racket/flonum (fl+ safe-fl+))
          safe-fl+)
         "mfqueue.rkt"
         "flipping.rkt"
         "transformation.rkt"
         "projection.rkt"
         "colors.rkt"
         racket/list
         "futures.rkt"
         "fixedpoint.rkt"
         racket/future
         "clipping.rkt"
         "image.rkt"
         "rendering-run.rkt"
         "pool.rkt"
         racket/format) ; for statistics

(define render-keep-ccw (make-parameter #t))
(define render-keep-cw (make-parameter #f))

(define-syntax (define-scan-triangle stx)
  (syntax-case stx ()
    ((_ (scan-triangle
         (x1 y1 coords1 ...)
         (x2 y2 coords2 ...)
         (x3 y3 coords3 ...)
         surface
         alpha)
        body ...)
     (with-syntax ((side-b-dy (datum->syntax stx 'side-b-dy))
                   (x1:fxp (datum->syntax stx 'x1:fxp))
                   (x2:fxp (datum->syntax stx 'x2:fxp))
                   (x3:fxp (datum->syntax stx 'x3:fxp))
                   (side-b-dx:fxp (datum->syntax stx 'side-b-dx:fxp))
                   (side-b-sx:fxp (datum->syntax stx 'side-b-sx:fxp))
                   )
       #'(define (scan-triangle x1 y1 coords1 ...
                                x2 y2 coords2 ...
                                x3 y3 coords3 ...
                                surface
                                (alpha #f))
           (when (fx< y2 y1)
             (swap-pairs2! (x1 y1 coords1 ...) (x2 y2 coords2 ...)))
           (when (fx< y3 y1)
             (swap-pairs2! (x1 y1 coords1 ...) (x3 y3 coords3 ...)))
           (when (fx< y3 y2)
             (swap-pairs2! (x2 y2 coords2 ...) (x3 y3 coords3 ...)))
           (define side-b-dy (fx- y3 y1))
           (when (fx> side-b-dy 0)
             (define x1:fxp (fx->fxp x1))
             (define x2:fxp (fx->fxp x2))
             (define x3:fxp (fx->fxp x3))
             (define side-b-dx:fxp (fx- x3:fxp x1:fxp))
             (define side-b-sx:fxp (fxquotient side-b-dx:fxp side-b-dy))
             body ...
             ))))))

(define-syntax (scan-triangle-invert-w! stx)
  (syntax-case stx ()
    ((_) #'(void))
    ((_ w rest ...)
     #'(begin
         (set! w (fl/ 1.0 w))
         (scan-triangle-invert-w! rest ...)))))

(define-syntax (scan-triangle-tex-invert-uvw! stx)
  (syntax-case stx ()
    ((_ tex u1 v1 w1 u2 v2 w2 u3 v3 w3)
     #'(let ((flimgwidth (texture-flwidth tex))
             (flimgheight (texture-flheight tex)))
         (set! u1 (fl/ (fl* u1 flimgwidth) w1))
         (set! v1 (fl/ (fl* v1 flimgheight) w1))
         (set! u2 (fl/ (fl* u2 flimgwidth) w2))
         (set! v2 (fl/ (fl* v2 flimgheight) w2))
         (set! u3 (fl/ (fl* u3 flimgwidth) w3))
         (set! v3 (fl/ (fl* v3 flimgheight) w3))
         (scan-triangle-invert-w! w1 w2 w3)
         ))))

(define-syntax (define-add-hline3 stx)
  (syntax-case stx ()
    ((_ (add-hline (width height hlines-queue allocate-hline hline-type tex fxs fls)
                   (vals ...) (steps ...)
                   (ivals ...) (isteps ...))
        body ...)
     #`(define (add-hline y x hw vals ... steps ...)
         (define-clip-scanline2 (clip-scanline width (vals ...) (steps ...)))
         (when (and (fx>= y 0)
                    (fx< y height))
           (let-values (((x hw vals ...) (clip-scanline x hw vals ... steps ...)))
             (when (fx> hw 0)
               (define hl (allocate-hline))
               (define off (fx+ (fx* y width) x))
               (define end-off (fx+ off hw))
               (set-hline-op! hl hline-type)
               (define fxs (hline-fxs hl))
               (fxvector-set! fxs 0 off)
               (fxvector-set! fxs 1 end-off)
               (fxvector-set! fxs 7 y)
               (set-hline-tex! hl tex)
               (define fls (hline-fls hl))
               #,@(flatten
                   (for/list ((val (syntax->list #'(vals ...)))
                              (step (syntax->list #'(steps ...)))
                              (ival (syntax->list #'(ivals ...)))
                              (istep (syntax->list #'(isteps ...))))
                     (if (is-it-fl? (syntax->datum val))
                         (list #`(flvector-set! fls #,ival #,val)
                               #`(flvector-set! fls #,istep #,step))
                         (list #`(fxvector-set! fxs #,ival #,val)
                               #`(fxvector-set! fxs #,istep #,step)))))
               body ...
               (mfqueue-enqueue! hlines-queue hl)
               )))))))

(define-syntax (define-process-scanline2 stx)
  (syntax-case stx ()
      ((_ (process-scanline (as ...) (bs ...)))
       (with-syntax ((add-hline (datum->syntax stx 'add-hline)))
         #`(define (process-scanline y ax:fxp as ... bx:fxp bs ...)
             (cond ((fx< ax:fxp bx:fxp)
                    (define ax (fxp->fx ax:fxp))
                    (define bx (fxp->fx bx:fxp))
                    (when (fx< ax bx)
                      (define width (fx- bx ax))
                      #,@(if (is-there-fl? (syntax->datum #'(as ...)))
                             #'((define width:fl (fx->fl width)))
                             '())
                      (add-hline y ax width
                                 as ...
                                 #,@(for/list ((a (syntax->list #'(as ...)))
                                               (b (syntax->list #'(bs ...))))
                                      (if (is-it-fl? (syntax->datum a))
                                          #`(fl/ (fl- #,b #,a) width:fl)
                                          #`(fxquotient (fx- #,b #,a) width))))))
                   ((fx< bx:fxp ax:fxp)
                    (process-scanline y bx:fxp bs ... ax:fxp as ...))))))))

(define-syntax (triangle-part-loop stx)
  (syntax-case stx ()
    ((_ y y2 (a-vals ...) (b-vals ...) (a-steps ...) (b-steps ...))
     (with-syntax ((process-scanline (datum->syntax stx 'process-scanline)))
       #`(let loop ((y y)
                    #,@(for/list ((val (syntax->list #'(a-vals ... b-vals ...))))
                         #`(#,val #,val)))
           (cond ((fx< y y2)
                  (process-scanline y a-vals ... b-vals ...)
                  (loop (fx+ y 1)
                        #,@(for/list ((val (syntax->list #'(a-vals ... b-vals ...)))
                                      (step (syntax->list #'(a-steps ... b-steps ...))))
                             (if (is-it-fl? (syntax->datum val))
                                 #`(fl+ #,val #,step)
                                 #`(fx+ #,val #,step)))))
                 (else
                  (values b-vals ...))))))))

(struct world-vertex
  (world)
  #:transparent)

(struct camera-vertex world-vertex
  (camera
   (stage #:mutable))
  #:transparent)

(struct pipeline-vertex camera-vertex
  (projection
   screen)
  #:transparent)

(define (make-camera-vertex x y z (w 1.0))
  (camera-vertex
   (vec4 x y z w)
   (make-vec4)
   0))

(define (make-pipeline-vertex x y z (w 1.0))
  (pipeline-vertex
   (vec4 x y z w)
   (make-vec4)
   0
   (make-vec4)
   (make-fxvector 2)))

(struct pipeline-triangle
  (vertices ; vector of pipeline-vertex - might be shared between multiple triangles, last one is midpoint! 4 elements
   uvcoords ; vector of 2D flvectors representing uv coordinates (of texture) - unique to this triangle
   surface ; color or texture
   type ; flat/textured
   alpha? ; ...
   normal ; calculated from camera-vertex-camera coordinates only after world->camera transformation
   orientation ; keep CCW, CW or both (#f)
   ; TODO: add something to keep track of frustrum clipping here
   )
  #:transparent)

(define (make-pipeline-triangle A B C surface type alpha? Auv Buv Cuv (ori 'ccw))
  (pipeline-triangle
   (vector A B C
           (make-pipeline-vertex 0 0 0))
   (vector Auv Buv Cuv)
   surface
   type
   alpha?
   (make-vec4)
   ori))

(define (render-add-triangle triangle)
  (pool-add! (if (pipeline-triangle-alpha? triangle)
                 (render-ctx-alpha-triangles (current-render-ctx))
                 (render-ctx-solid-triangles (current-render-ctx)))
             triangle))

(define (render-create-triangle A B C surface type alpha? Auv Buv Cuv (ori 'ccw))
  (render-add-triangle
   (make-pipeline-triangle A B C surface type alpha? Auv Buv Cuv ori)))

(define (rcge-3d-add-triangle A B C
                              surface
                              (type 'flat)
                              (AT #f) (BT #f) (CT #f))
  (render-create-triangle A B C surface type
                          (case type
                            ((flat/a lit/a textured/a texlit/a) #t)
                            (else #f))
                          AT BT CT))

(define (rcge-3d-camera x y z ay ax az)
  (define ctx (current-render-ctx))
  (mat4*mat4!
   (render-ctx-camera-matrix ctx)
   (mat4-rot3d ax ay az)
   (mat4-mov3d (fl- x) (fl- y) (fl- z))))

(define (rcge-3d-projection fov near far (aspect #f) #:pixel-aspect (pixel-aspect #f))
  (define ctx (current-render-ctx))
  (mat4-project!
   (render-ctx-projection-matrix ctx)
   fov near far aspect pixel-aspect))

(define (rcge-3d-show-borders! (b 'toggle))
  (define ctx (current-render-ctx))
  (case b
    ((toggle)
     (set-render-ctx-show-borders!
      ctx
      (not (render-ctx-show-borders ctx))))
    (else (set-render-ctx-show-borders! ctx b))))

(struct rcge-light
  (
   vertex
   intensity
   anglemul
   distmul
   )
  #:transparent
  )

(define (make-rcge-light x y z intensity (anglemul 0.9) (distmul 0.25))
  (rcge-light
   (make-camera-vertex x y z)
   intensity
   anglemul
   distmul))

(define (rcge-add-light l1 l2)
  (fx+ l1
       (fxquotient (fx* (fx- rcge-light-inf l1)
                        l2)
                   rcge-light-inf)))

(define (rcge-vertex-light vertex norm lights)
  (define V (camera-vertex-camera vertex))
  (define normsize (flsqrt (vec3.vec3 norm norm)))
  (let loop ((l 0)
             (lights lights))
    (cond
      ((empty? lights)
       l)
      (else
       (define light (car lights))
       (define anglemul (rcge-light-anglemul light))
       (define distmul (rcge-light-distmul light))
       (define R (vec3-vec3 V (camera-vertex-camera (rcge-light-vertex light))))
       (define d (flsqrt (vec3.vec3 R R)))
       (define costheta (flabs (fl/ (vec3.vec3 norm R)
                                    (fl* d normsize))))
       ;(displayln costheta)
       (define l2 (fl->fx (fl* (fl+ (fl- 1.0 anglemul)
                                    (fl* costheta anglemul))
                               (fl/ rcge-light-inf:fl
                                    (fl+ 1.0 (fl* d distmul))))))
       (loop (rcge-add-light l l2)
             (cdr lights))))))

(define (rcge-add-light-source x y z i)
  (define rctx (current-render-ctx))
  (set-render-ctx-lights!
   rctx
   (cons
    (make-rcge-light x y z i)
    (render-ctx-lights rctx)))
  (void))

(define-syntax (define-draw-proc-variants-w-buffer stx)
  (syntax-case stx ()
    ((_ definer basename)
     (with-syntax ((name #'basename)
                   (name/a (datum->syntax stx (string->symbol (~a (syntax->datum #'basename) "/a"))))
                   (w-buffer (datum->syntax stx 'w-buffer)))
       #'(begin
           (definer (name (color 0))
             ((define color32 (fxior argb-alpha-mask color)))
             (when (fl> w (flvector-ref w-buffer off))
               (flvector-set! w-buffer off w)
               (fxvector-set! framebuffer off color32)))
           (definer (name/a (argb argb-alpha-mask))
             ((define a2 (fxand (fxrshift argb 8) #xff0000))
              (define r2 (fxand (fxrshift argb 16) 255))
              (define g2 (fxand (fxrshift argb 8) 255))
              (define b2 (fxand argb 255)))
             (define rgb (fxvector-ref framebuffer off))
             (define r1 (fxand (fxrshift rgb 8) #xff00))
             (define g1 (fxand rgb #xff00))
             (define b1 (fxand (fxlshift rgb 8) #xff00))
             (fxvector-set! framebuffer off
                            (fxior argb-alpha-mask
                                   (fxlshift (bytes-ref rcge-blend-table (fxior a2 r1 r2)) 16)
                                   (fxlshift (bytes-ref rcge-blend-table (fxior a2 g1 g2)) 8)
                                   (bytes-ref rcge-blend-table (fxior a2 b1 b2))))))))))

(define-syntax (template-line-w-buffer stx)
  (syntax-case stx ()
    ((_ (name args ...) (prepare ...) body ...)
     (with-syntax ((framebuffer (datum->syntax stx 'framebuffer))
                   (off (datum->syntax stx 'off))
                   (w (datum->syntax stx 'w)))
       #'(define (name x0 y0 w0 x1 y1 w1 args ...)
           (define-values (width height framebuffer) (current-width-height-framebuffer))
           prepare ...
           (define (line-low x0 y0 w0 x1 y1 w1)
             (define dx (fx- x1 x0))
             (define dw (fl- w1 w0))
             (define-values (dy yi)
               (if (fx< y1 y0)
                   (values (fx- y0 y1) -1)
                   (values (fx- y1 y0) 1)))
             (define wi (fl/ dw (fx->fl dx)))
             (define yoffd (fx* yi width))
             (let loop ((x x0)
                        (y y0)
                        (off (fx+ (fx* width y0) x0))
                        (D (fx- (fxlshift dy 1) dx))
                        (w w0))
               (when (fx<= x x1)
                 (when (and (fx>= x 0)
                            (fx>= y 0)
                            (fx< x width)
                            (fx< y height))
                   body ...)
                 (if (fx> D 0)
                     (loop (fx+ x 1)
                           (fx+ y yi)
                           (fx+ off yoffd 1)
                           (fx+ (fx- D (fxlshift dx 1)) (fxlshift dy 1))
                           (fl+ w wi))
                     (loop (fx+ x 1)
                           y
                           (fx+ off 1)
                           (fx+ D (fxlshift dy 1))
                           (fl+ w wi))))))
           (define (line-high x0 y0 w0 x1 y1 w1)
             (define dy (fx- y1 y0))
             (define dw (fl- w1 w0))
             (define-values (dx xi)
               (if (fx< x1 x0)
                   (values (fx- x0 x1) -1)
                   (values (fx- x1 x0) 1)))
             (define wi (fl/ dw (fx->fl dy)))
             (let loop ((y y0)
                        (x x0)
                        (off (fx+ (fx* width y0) x0))
                        (D (fx- (fxlshift dx 1) dy))
                        (w w0))
               (when (fx<= y y1)
                 (when (and (fx>= x 0)
                            (fx>= y 0)
                            (fx< x width)
                            (fx< y height))
                   body ...)
                 (if (fx> D 0)
                     (loop (fx+ y 1)
                           (fx+ x xi)
                           (fx+ off xi width)
                           (fx+ (fx- D (fxlshift dy 1)) (fxlshift dx 1))
                           (fl+ w wi))
                     (loop (fx+ y 1)
                           x
                           (fx+ off width)
                           (fx+ D (fxlshift dx 1))
                           (fl+ w wi))))))
           (if (fx< (fxabs (fx- y1 y0))
                    (fxabs (fx- x1 x0)))
               (if (fx> x0 x1)
                   (line-low x1 y1 w1 x0 y0 w0)
                   (line-low x0 y0 w0 x1 y1 w1))
               (if (fx> y0 y1)
                   (line-high x1 y1 w1 x0 y0 w0)
                   (line-high x0 y0 w0 x1 y1 w1)))
           (void))))))

(define (scene-render)
  (define max-depth (rcge-futures-depth))
  (define num-futures (expt 2 max-depth))
  (define counters (make-fxvector num-futures))
  (define-values (width height framebuffer) (current-width-height-framebuffer))
  (define rctx (current-render-ctx))
  (define w-buffer (render-ctx-w-buffer rctx))
  (define hlines-pool (render-ctx-hlines-pool rctx))
  (define hlines-queue (render-ctx-hlines-queue rctx))

  (define hlines-pool-idx 0)
  (define hlines-pool-size (vector-length hlines-pool))

  (define-draw-proc-variants-w-buffer template-line-w-buffer draw-line-w-buffer)
  
  (define (clear-w-buffer)
    (define-futurized (do-clear start end)
      (let loop ((off start))
        (when (fx< off end)
          (flvector-set! w-buffer off 0.0)
          (loop (fx+ off 1)))))
    (do-clear 0 (fx* width height)))

  (clear-w-buffer)

  (define (do-hline-fill action)
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define color (hline-tex action))
    (define color2 (fxior argb-alpha-mask color))
    (let xloop ((off (fxvector-ref fxs 0)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (fxvector-set! framebuffer off color2)
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               ))))
  (define (do-hline-fill/a action)
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define argb (hline-tex action))
    (define a2 (fxand (fxrshift argb 8) #xff0000))
    (define r2 (fxand (fxrshift argb 16) 255))
    (define g2 (fxand (fxrshift argb 8) 255))
    (define b2 (fxand argb 255))
    (let xloop ((off (fxvector-ref fxs 0)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define rgb (fxvector-ref framebuffer off))
          (define r1 (fxand (fxrshift rgb 8) #xff00))
          (define g1 (fxand rgb #xff00))
          (define b1 (fxand (fxlshift rgb 8) #xff00))
          (fxvector-set! framebuffer off
                         (fxior argb-alpha-mask
                                (fxlshift (bytes-ref rcge-blend-table (fxior a2 r1 r2)) 16)
                                (fxlshift (bytes-ref rcge-blend-table (fxior a2 g1 g2)) 8)
                                (bytes-ref rcge-blend-table (fxior a2 b1 b2))))
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               ))))
  (define (do-hline-fill/l action)
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define argb (hline-tex action))
    (define sl:fxp (fxvector-ref fxs 3))
    (let xloop ((off (fxvector-ref fxs 0))
                (l:fxp (fxvector-ref fxs 2)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (fxvector-set! framebuffer off (rcge-light-color1 argb (fxp->fx l:fxp)))
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               (fx+ l:fxp sl:fxp)))))
  (define (do-hline-fill/a/l action)
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define argb (hline-tex action))
    (define sl:fxp (fxvector-ref fxs 3))
    (let xloop ((off (fxvector-ref fxs 0))
                (l:fxp (fxvector-ref fxs 2)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define argb2 (rcge-light-color1 argb (fxp->fx l:fxp)))
          (define a2 (fxand (fxrshift argb 8) #xff0000))
          (define r2 (fxand (fxrshift argb2 16) 255))
          (define g2 (fxand (fxrshift argb2 8) 255))
          (define b2 (fxand argb2 255))
          (define rgb (fxvector-ref framebuffer off))
          (define r1 (fxand (fxrshift rgb 8) #xff00))
          (define g1 (fxand rgb #xff00))
          (define b1 (fxand (fxlshift rgb 8) #xff00))
          (fxvector-set! framebuffer off
                         (fxior argb-alpha-mask
                                (fxlshift (bytes-ref rcge-blend-table (fxior a2 r1 r2)) 16)
                                (fxlshift (bytes-ref rcge-blend-table (fxior a2 g1 g2)) 8)
                                (bytes-ref rcge-blend-table (fxior a2 b1 b2))))
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               (fx+ l:fxp sl:fxp)))))
  (define (do-hline-tex action)
    (define imgdata (hline-tex action))
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define imgwidthmask (fxvector-ref fxs 4))
    (define imgheightmask (fxvector-ref fxs 5))
    (define imglog2width (fxvector-ref fxs 6))
    (let xloop ((off (fxvector-ref fxs 0)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define uw (fxand (fl->fx (fl/ (flvector-ref fls 0) (flvector-ref fls 2))) imgwidthmask))
          (define vh (fxand (fl->fx (fl/ (flvector-ref fls 1) (flvector-ref fls 2))) imgheightmask))
          (define imgoff (fx+ (fxlshift vh imglog2width) uw))
          (fxvector-set! framebuffer off (fxvector-ref imgdata imgoff))
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 0 (fl+ (flvector-ref fls 0) (flvector-ref fls 3)))
        (flvector-set! fls 1 (fl+ (flvector-ref fls 1) (flvector-ref fls 4)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               ))))
  (define (do-hline-tex/a action)
    (define imgdata (hline-tex action))
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define imgwidthmask (fxvector-ref fxs 4))
    (define imgheightmask (fxvector-ref fxs 5))
    (define imglog2width (fxvector-ref fxs 6))
    (let xloop ((off (fxvector-ref fxs 0)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define uw (fxand (fl->fx (fl/ (flvector-ref fls 0) (flvector-ref fls 2))) imgwidthmask))
          (define vh (fxand (fl->fx (fl/ (flvector-ref fls 1) (flvector-ref fls 2))) imgheightmask))
          (define imgoff (fx+ (fxlshift vh imglog2width) uw))
          (define argb (fxvector-ref imgdata imgoff))
          (define a2 (fxand (fxrshift argb 8) #xff0000))
          (when (fx> a2 0)
            (define/splice-r2-g2-b2-rgb-r1-g1-b1-nrgb)
            (when (not (fx= nrgb rgb))
              (fxvector-set! framebuffer off nrgb)
              (flvector-set! w-buffer off (flvector-ref fls 2)))))
        (flvector-set! fls 0 (fl+ (flvector-ref fls 0) (flvector-ref fls 3)))
        (flvector-set! fls 1 (fl+ (flvector-ref fls 1) (flvector-ref fls 4)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               ))))
  (define (do-hline-tex/l action)
    (define imgdata (hline-tex action))
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define sl:fxp (fxvector-ref fxs 3))
    (define imgwidthmask (fxvector-ref fxs 4))
    (define imgheightmask (fxvector-ref fxs 5))
    (define imglog2width (fxvector-ref fxs 6))
    ;(define su (flvector-ref fls 3))
    ;(define sv (flvector-ref fls 4))
    ;(define sw (flvector-ref fls 5))
    (let xloop ((off (fxvector-ref fxs 0))
                (l:fxp (fxvector-ref fxs 2))
                (w (safe-fl+ (flvector-ref fls 2)))
                (sw (safe-fl+ (flvector-ref fls 5)))
                (u (safe-fl+ (flvector-ref fls 0)))
                (su (safe-fl+ (flvector-ref fls 3)))
                (v (safe-fl+ (flvector-ref fls 1)))
                (sv (safe-fl+ (flvector-ref fls 4)))
                )
      (when (fx< off end-off)
        (when (fl> w (flvector-ref w-buffer off))
          (define uw (fxand (fl->fx (fl/ u w)) imgwidthmask))
          (define vh (fxand (fl->fx (fl/ v w)) imgheightmask))
          (define imgoff (fx+ (fxlshift vh imglog2width) uw))
          (fxvector-set! framebuffer off (rcge-light-color1 (fxvector-ref imgdata imgoff) (fxp->fx l:fxp)))
          (flvector-set! w-buffer off w))
        ;(flvector-set! fls 0 (fl+ (flvector-ref fls 0) (flvector-ref fls 3)))
        ;(flvector-set! fls 1 (fl+ (flvector-ref fls 1) (flvector-ref fls 4)))
        ;(flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               (fx+ l:fxp sl:fxp)
               (fl+ w sw)
               sw
               (fl+ u su)
               su
               (fl+ v sv)
               sv
               )))
    #;(let xloop ((off (fxvector-ref fxs 0))
                (l:fxp (fxvector-ref fxs 2)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define uw (fxand (fl->fx (fl/ (flvector-ref fls 0) (flvector-ref fls 2))) imgwidthmask))
          (define vh (fxand (fl->fx (fl/ (flvector-ref fls 1) (flvector-ref fls 2))) imgheightmask))
          (define imgoff (fx+ (fxlshift vh imglog2width) uw))
          (fxvector-set! framebuffer off (rcge-light-color1 (fxvector-ref imgdata imgoff) (fxp->fx l:fxp)))
          (flvector-set! w-buffer off (flvector-ref fls 2)))
        (flvector-set! fls 0 (fl+ (flvector-ref fls 0) (flvector-ref fls 3)))
        (flvector-set! fls 1 (fl+ (flvector-ref fls 1) (flvector-ref fls 4)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               (fx+ l:fxp sl:fxp)
               )))
    )
  (define (do-hline-tex/a/l action)
    (define imgdata (hline-tex action))
    (define fxs (hline-fxs action))
    (define fls (hline-fls action))
    (define end-off (fxvector-ref fxs 1))
    (define sl:fxp (fxvector-ref fxs 3))
    (define imgwidthmask (fxvector-ref fxs 4))
    (define imgheightmask (fxvector-ref fxs 5))
    (define imglog2width (fxvector-ref fxs 6))
    (let xloop ((off (fxvector-ref fxs 0))
                (l:fxp (fxvector-ref fxs 2)))
      (when (fx< off end-off)
        (when (fl> (flvector-ref fls 2) (flvector-ref w-buffer off))
          (define uw (fxand (fl->fx (fl/ (flvector-ref fls 0) (flvector-ref fls 2))) imgwidthmask))
          (define vh (fxand (fl->fx (fl/ (flvector-ref fls 1) (flvector-ref fls 2))) imgheightmask))
          (define imgoff (fx+ (fxlshift vh imglog2width) uw))
          (define argb2 (fxvector-ref imgdata imgoff))
          (define a2 (fxand (fxrshift argb2 8) #xff0000))
          (when (fx> a2 0)
            (define argb (rcge-light-color1 argb2 (fxp->fx l:fxp)))
            (define/splice-r2-g2-b2-rgb-r1-g1-b1-nrgb)
            (when (not (fx= nrgb rgb))
              (fxvector-set! framebuffer off nrgb)
              (flvector-set! w-buffer off (flvector-ref fls 2)))))
        (flvector-set! fls 0 (fl+ (flvector-ref fls 0) (flvector-ref fls 3)))
        (flvector-set! fls 1 (fl+ (flvector-ref fls 1) (flvector-ref fls 4)))
        (flvector-set! fls 2 (fl+ (flvector-ref fls 2) (flvector-ref fls 5)))
        (xloop (fx+ off 1)
               (fx+ l:fxp sl:fxp)
               ))))

  (define main-hline-worker-sema (make-fsemaphore 0))
  (define scanline-locks (render-ctx-scanline-locks rctx))

  (define (start-hline-workers (depth 0) (fid 0))
    (cond ((fx< depth max-depth)
           (let ((f (future
                     (λ ()
                       (start-hline-workers (fx+ depth 1) (fxior fid (fxlshift 1 depth)))))))
             (start-hline-workers (fx+ depth 1) fid)
             (touch f)))
          (else
           (when (fx= fid 0)
             (fsemaphore-wait main-hline-worker-sema))
           (let loop ()
             (define action (mfqueue-dequeue! hlines-queue))
             (when action ; stops on #f
               (define y (fxvector-ref (hline-fxs action) 7))
               (let lloop ()
                 (when (not (vector*-cas! scanline-locks y #f #t))
                   (lloop)))
               (fxvector-set! counters fid (fx+ (fxvector-ref counters fid) 1))
               (case (hline-op action)
                 ((hline-fill) (do-hline-fill action))
                 ((hline-fill/a) (do-hline-fill/a action))
                 ((hline-fill/l) (do-hline-fill/l action))
                 ((hline-fill/a/l) (do-hline-fill/a/l action))
                 ((hline-tex) (do-hline-tex action))
                 ((hline-tex/a) (do-hline-tex/a action))
                 ((hline-tex/l) (do-hline-tex/l action))
                 ((hline-tex/a/l) (do-hline-tex/a/l action)))
               (vector-set! scanline-locks y #f)
               (loop))))))

  ; Project all triangles
  (define keep-ccw (render-keep-ccw))
  (define keep-cw (render-keep-cw))
  (define stage (fx+ (render-ctx-stage rctx) 2))
  (define stage1 (fx+ stage 1))
  (set-render-ctx-stage! rctx stage)
  (define projected-triangles (render-ctx-render-triangles rctx))
  (pool-reset! projected-triangles)
  (define solid-triangles (render-ctx-solid-triangles rctx))
  (define alpha-triangles (render-ctx-alpha-triangles rctx))
  (define tmp-u (make-vec4))
  (define tmp-v (make-vec4))
  (define vec4->vec2! (make-vec4->vec2!))
  (define camera-matrix (render-ctx-camera-matrix rctx))
  (define projection-matrix (render-ctx-projection-matrix rctx))
  ; TODO: add another pool for adding all triangles while clipping against frustrum at **1** / maybe use projected-triangles
  ; Keep how many solid triangles were added
  ; after solid and alpha triangles were added (including clipping), sort first part front->back, second part back->front **2**
  ; only after this project all the triangles
  (define num-solid #f)
  (for ((triangles (list solid-triangles
                         alpha-triangles)))
    (pool-for-each
     triangles
     (λ (triangle)
       (define points (pipeline-triangle-vertices triangle))

       ; Transform its coordinates to camera space - unless it has already been performed for another triangle
       (for ((A points))
         (when (not (fx= stage (camera-vertex-stage A)))
           (mat4*vec4! (camera-vertex-camera A)
                       camera-matrix
                       (world-vertex-world A))
           (set-camera-vertex-stage! A stage)))

       ; Calculate camera space normal
       (define A (vector-ref points 0))
       (define B (vector-ref points 1))
       (define C (vector-ref points 2))
       (define Aw (camera-vertex-camera A))
       (define Bw (camera-vertex-camera B))
       (define Cw (camera-vertex-camera C))
       (vec3-vec3! tmp-u Bw Aw)
       (vec3-vec3! tmp-v Cw Aw)
       (define tmp-n (pipeline-triangle-normal triangle))
       (vec3*vec3! tmp-n tmp-u tmp-v)

       ; Keep or drop
       (when (or (and keep-ccw (fl< (vec3.vec3 tmp-n Aw) 0.0))
                 (and keep-cw (fl> (vec3.vec3 tmp-n Aw) 0.0)))
         
         ; Compute midpoint (must be reworked after frustrum clipping is done)
         (define M (vector-ref points 3))
         (define Mc (camera-vertex-camera M))
         (flvector-set! Mc 0
                        (fl/ (fl+ (flvector-ref Aw 0)
                                  (flvector-ref Bw 0)
                                  (flvector-ref Cw 0))
                             3.0))
         (flvector-set! Mc 1
                       (fl/ (fl+ (flvector-ref Aw 1)
                                 (flvector-ref Bw 1)
                                 (flvector-ref Cw 1))
                            3.0))
         (flvector-set! Mc 2
                       (fl/ (fl+ (flvector-ref Aw 2)
                                 (flvector-ref Bw 2)
                                 (flvector-ref Cw 2))
                            3.0))
         ; **1** - if clipping happens, just add the new triangles to the triangles pool
         (for ((A points))
           (when (not (fx= stage1 (camera-vertex-stage A)))
             (mat4*vec4! (pipeline-vertex-projection A)
                         projection-matrix
                         (camera-vertex-camera A))
             (vec4-project! (pipeline-vertex-projection A)
                            (pipeline-vertex-projection A))
             (vec4->vec2! (pipeline-vertex-screen A)
                          (pipeline-vertex-projection A))
             (set-camera-vertex-stage! A stage1)
             ))
         (pool-add! projected-triangles triangle))))
    (when (not num-solid)
      (set! num-solid (pool-size projected-triangles))))
  ; **2** - sorting and projecting at this stage

  (define (allocate-hline)
    (when (fx= hlines-pool-size hlines-pool-idx)
      (define new-size (fx+ hlines-pool-size hlines-pool-grow-step))
      (set! hlines-pool
            (for/vector #:length new-size ((i new-size))
              (if (fx< i hlines-pool-size)
                  (vector-ref hlines-pool i)
                  (make-hline))))
      (set! hlines-pool-size new-size)
      (set-render-ctx-hlines-pool! rctx hlines-pool))
    (define hl (vector-ref hlines-pool hlines-pool-idx))
    (set! hlines-pool-idx (add1 hlines-pool-idx))
    hl)

  (define-scan-triangle (scan-triangle-fill (x1 y1 w1)
                                            (x2 y2 w2)
                                            (x3 y3 w3)
                                            argb
                                            alpha)
    (scan-triangle-invert-w! w1 w2 w3)
    (define color (if alpha argb (fxior argb argb-alpha-mask)))
    (define hline-type (if alpha 'hline-fill/a 'hline-fill))
    (define-add-hline3 (add-hline (width height hlines-queue allocate-hline hline-type color fxs fls)
                                  (w:fl) (sw:fl)
                                  (2) (5)))
    (define-process-scanline2 (process-scanline (aw:fl) (bw:fl)))
    (define side-a-dy (fx- y2 y1)) ; must stay
    (define side-b-dw:fl (fl- w3 w1))
    (define side-b-dy:fl (fx->fl side-b-dy))
    (define side-b-sw:fl (fl/ side-b-dw:fl side-b-dy:fl))
    (define (triangle-part y y2
                           side-a-x:fxp side-b-x:fxp side-a-dx:fxp side-a-dy
                           side-a-w:fl side-b-w:fl side-a-dw:fl)
      (define side-a-dy:fl (fx->fl side-a-dy))
      (define side-a-sx:fxp (fxquotient side-a-dx:fxp side-a-dy))
      (define side-a-sw:fl (fl/ side-a-dw:fl side-a-dy:fl))
      (triangle-part-loop y y2
                          (side-a-x:fxp side-a-w:fl)
                          (side-b-x:fxp side-b-w:fl)
                          (side-a-sx:fxp side-a-sw:fl)
                          (side-b-sx:fxp side-b-sw:fl)))
    (define-values (side-b-x:fxp side-b-w:fl)
      (cond
        ((fx> side-a-dy 0)
         (define side-a-dw:fl (fl- w2 w1))
         (define side-a-dx:fxp (fx- x2:fxp x1:fxp))
         (triangle-part y1 y2
                        x1:fxp x1:fxp side-a-dx:fxp side-a-dy
                        w1 w1 side-a-dw:fl))
        (else
         (values x1:fxp w1))))
    (define side-c-dy (fx- y3 y2))
    (when (fx> side-c-dy 0)
      (define side-c-dw:fl (fl- w3 w2))
      (define side-c-dx:fxp (fx- x3:fxp x2:fxp))
      (triangle-part y2 y3
                     x2:fxp side-b-x:fxp side-c-dx:fxp side-c-dy
                     w2 side-b-w:fl side-c-dw:fl)))

  (define-scan-triangle (scan-triangle-fill/l (x1 y1 w1 l1)
                                              (x2 y2 w2 l2)
                                              (x3 y3 w3 l3)
                                              argb
                                              alpha)
    (scan-triangle-invert-w! w1 w2 w3)
    (define color (if alpha argb (fxior argb argb-alpha-mask)))
    (define hline-type (if alpha 'hline-fill/a/l 'hline-fill/l))
    (define-add-hline3 (add-hline (width height hlines-queue allocate-hline hline-type color fxs fls)
                                  (w:fl l:fxp) (sw:fl sl:fxp)
                                  (2 2) (5 3)))
    (define-process-scanline2 (process-scanline (aw:fl al:fxp) (bw:fl bl:fxp)))
    (define side-a-dy (fx- y2 y1))
    (define side-b-dw:fl (fl- w3 w1))
    (define side-b-dy:fl (fx->fl side-b-dy))
    (define side-b-sw:fl (fl/ side-b-dw:fl side-b-dy:fl))
    (define l1:fxp (fx->fxp (rcge-clip-light l1)))
    (define l2:fxp (fx->fxp (rcge-clip-light l2)))
    (define l3:fxp (fx->fxp (rcge-clip-light l3)))
    (define side-b-dl:fxp (fx- l3:fxp l1:fxp))
    (define side-b-sl:fxp (fxquotient side-b-dl:fxp side-b-dy))
    (define (triangle-part y y2
                           side-a-x:fxp side-b-x:fxp side-a-dx:fxp side-a-dy
                           side-a-w:fl side-b-w:fl side-a-dw:fl
                           side-a-l:fxp side-b-l:fxp side-a-dl:fxp)
      (define side-a-dy:fl (fx->fl side-a-dy))
      (define side-a-sx:fxp (fxquotient side-a-dx:fxp side-a-dy))
      (define side-a-sw:fl (fl/ side-a-dw:fl side-a-dy:fl))
      (define side-a-sl:fxp (fxquotient side-a-dl:fxp side-a-dy))
      (triangle-part-loop y y2
                          (side-a-x:fxp side-a-w:fl side-a-l:fxp)
                          (side-b-x:fxp side-b-w:fl side-b-l:fxp)
                          (side-a-sx:fxp side-a-sw:fl side-a-sl:fxp)
                          (side-b-sx:fxp side-b-sw:fl side-b-sl:fxp)))
    (define-values (side-b-x:fxp side-b-w:fl side-b-l:fxp)
      (cond
        ((fx> side-a-dy 0)
         (define side-a-dw:fl (fl- w2 w1))
         (define side-a-dx:fxp (fx- x2:fxp x1:fxp))
         (define side-a-dl:fxp (fx- l2:fxp l1:fxp))
         (triangle-part y1 y2
                        x1:fxp x1:fxp side-a-dx:fxp side-a-dy
                        w1 w1 side-a-dw:fl
                        l1:fxp l1:fxp side-a-dl:fxp))
        (else
         (values x1:fxp w1 l1:fxp))))
    (define side-c-dy (fx- y3 y2))
    (when (fx> side-c-dy 0)
      (define side-c-dw:fl (fl- w3 w2))
      (define side-c-dx:fxp (fx- x3:fxp x2:fxp))
      (define side-c-dl:fxp (fx- l3:fxp l2:fxp))
      (triangle-part y2 y3
                     x2:fxp side-b-x:fxp side-c-dx:fxp side-c-dy
                     w2 side-b-w:fl side-c-dw:fl
                     l2:fxp side-b-l:fxp side-c-dl:fxp)))

  (define-scan-triangle (scan-triangle-tex (x1 y1 u1 v1 w1)
                                           (x2 y2 u2 v2 w2)
                                           (x3 y3 u3 v3 w3)
                                           tex
                                           alpha)
    (scan-triangle-tex-invert-uvw! tex u1 v1 w1 u2 v2 w2 u3 v3 w3)
    (define hline-type (if alpha 'hline-tex/a 'hline-tex))
    (define-add-hline3 (add-hline (width height hlines-queue allocate-hline hline-type (image-data tex) fxs fls)
                                  (u:fl v:fl w:fl)
                                  (su:fl sv:fl sw:fl)
                                  (0 1 2) (3 4 5))
      (fxvector-set! fxs 4 (texture-widthmask tex))
      (fxvector-set! fxs 5 (texture-heightmask tex))
      (fxvector-set! fxs 6 (texture-log2width tex)))
    (define-process-scanline2 (process-scanline (au:fl av:fl aw:fl) (bu:fl bv:fl bw:fl)))
    (define side-a-dy (fx- y2 y1)) ; must stay
    (define side-b-du:fl (fl- u3 u1))
    (define side-b-dv:fl (fl- v3 v1))
    (define side-b-dw:fl (fl- w3 w1))
    (define side-b-dy:fl (fx->fl side-b-dy))
    (define side-b-su:fl (fl/ side-b-du:fl side-b-dy:fl))
    (define side-b-sv:fl (fl/ side-b-dv:fl side-b-dy:fl))
    (define side-b-sw:fl (fl/ side-b-dw:fl side-b-dy:fl))
    (define (triangle-part y y2
                           side-a-x:fxp side-b-x:fxp side-a-dx:fxp side-a-dy
                           side-a-u:fl side-a-v:fl side-a-w:fl
                           side-b-u:fl side-b-v:fl side-b-w:fl
                           side-a-du:fl side-a-dv:fl side-a-dw:fl)
      (define side-a-dy:fl (fx->fl side-a-dy))
      (define side-a-sx:fxp (fxquotient side-a-dx:fxp side-a-dy))
      (define side-a-su:fl (fl/ side-a-du:fl side-a-dy:fl))
      (define side-a-sv:fl (fl/ side-a-dv:fl side-a-dy:fl))
      (define side-a-sw:fl (fl/ side-a-dw:fl side-a-dy:fl))
      (triangle-part-loop y y2
                          (side-a-x:fxp side-a-u:fl side-a-v:fl side-a-w:fl)
                          (side-b-x:fxp side-b-u:fl side-b-v:fl side-b-w:fl)
                          (side-a-sx:fxp side-a-su:fl side-a-sv:fl side-a-sw:fl)
                          (side-b-sx:fxp side-b-su:fl side-b-sv:fl side-b-sw:fl)))
    (define-values (side-b-x:fxp side-b-u:fl side-b-v:fl side-b-w:fl)
      (cond
        ((fx> side-a-dy 0)
         (define side-a-du:fl (fl- u2 u1))
         (define side-a-dv:fl (fl- v2 v1))
         (define side-a-dw:fl (fl- w2 w1))
         (define side-a-dx:fxp (fx- x2:fxp x1:fxp))
         (triangle-part y1 y2
                        x1:fxp x1:fxp side-a-dx:fxp side-a-dy
                        u1 v1 w1
                        u1 v1 w1
                        side-a-du:fl side-a-dv:fl side-a-dw:fl))
        (else
         (values x1:fxp u1 v1 w1))))
    (define side-c-dy (fx- y3 y2))
    (when (fx> side-c-dy 0)
      (define side-c-du:fl (fl- u3 u2))
      (define side-c-dv:fl (fl- v3 v2))
      (define side-c-dw:fl (fl- w3 w2))
      (define side-c-dx:fxp (fx- x3:fxp x2:fxp))
      (triangle-part y2 y3
                     x2:fxp side-b-x:fxp side-c-dx:fxp side-c-dy
                     u2 v2 w2
                     side-b-u:fl side-b-v:fl side-b-w:fl
                     side-c-du:fl side-c-dv:fl side-c-dw:fl)))

  (define-scan-triangle (scan-triangle-tex/l (x1 y1 u1 v1 w1 l1)
                                             (x2 y2 u2 v2 w2 l2)
                                             (x3 y3 u3 v3 w3 l3)
                                             tex
                                             alpha)
    (scan-triangle-tex-invert-uvw! tex u1 v1 w1 u2 v2 w2 u3 v3 w3)
    (define hline-type (if alpha 'hline-tex/a/l 'hline-tex/l))
    (define-add-hline3 (add-hline (width height hlines-queue allocate-hline hline-type (image-data tex) fxs fls)
                                 (u:fl v:fl w:fl l:fxp)
                                 (su:fl sv:fl sw:fl sl:fxp)
                                 (0 1 2 2) (3 4 5 3))
      (fxvector-set! fxs 4 (texture-widthmask tex))
      (fxvector-set! fxs 5 (texture-heightmask tex))
      (fxvector-set! fxs 6 (texture-log2width tex)))
    (define-process-scanline2 (process-scanline (au:fl av:fl aw:fl al:fxp) (bu:fl bv:fl bw:fl bl:fxp)))
    (define side-a-dy (fx- y2 y1)) ; must stay
    (define side-b-du:fl (fl- u3 u1))
    (define side-b-dv:fl (fl- v3 v1))
    (define side-b-dw:fl (fl- w3 w1))
    (define side-b-dy:fl (fx->fl side-b-dy))
    (define side-b-su:fl (fl/ side-b-du:fl side-b-dy:fl))
    (define side-b-sv:fl (fl/ side-b-dv:fl side-b-dy:fl))
    (define side-b-sw:fl (fl/ side-b-dw:fl side-b-dy:fl))
    (define l1:fxp (fx->fxp (rcge-clip-light l1)))
    (define l2:fxp (fx->fxp (rcge-clip-light l2)))
    (define l3:fxp (fx->fxp (rcge-clip-light l3)))
    (define side-b-dl:fxp (fx- l3:fxp l1:fxp))
    (define side-b-sl:fxp (fxquotient side-b-dl:fxp side-b-dy))
    (define (triangle-part y y2
                           side-a-x:fxp side-b-x:fxp side-a-dx:fxp side-a-dy
                           side-a-u:fl side-a-v:fl side-a-w:fl side-a-l:fxp
                           side-b-u:fl side-b-v:fl side-b-w:fl side-b-l:fxp
                           side-a-du:fl side-a-dv:fl side-a-dw:fl side-a-dl:fxp)
      (define side-a-dy:fl (fx->fl side-a-dy))
      (define side-a-sx:fxp (fxquotient side-a-dx:fxp side-a-dy))
      (define side-a-su:fl (fl/ side-a-du:fl side-a-dy:fl))
      (define side-a-sv:fl (fl/ side-a-dv:fl side-a-dy:fl))
      (define side-a-sw:fl (fl/ side-a-dw:fl side-a-dy:fl))
      (define side-a-sl:fxp (fxquotient side-a-dl:fxp side-a-dy))
      (triangle-part-loop y y2
                          (side-a-x:fxp side-a-u:fl side-a-v:fl side-a-w:fl side-a-l:fxp)
                          (side-b-x:fxp side-b-u:fl side-b-v:fl side-b-w:fl side-b-l:fxp)
                          (side-a-sx:fxp side-a-su:fl side-a-sv:fl side-a-sw:fl side-a-sl:fxp)
                          (side-b-sx:fxp side-b-su:fl side-b-sv:fl side-b-sw:fl side-b-sl:fxp)))
    (define-values (side-b-x:fxp side-b-u:fl side-b-v:fl side-b-w:fl side-b-l:fxp)
      (cond
        ((fx> side-a-dy 0)
         (define side-a-du:fl (fl- u2 u1))
         (define side-a-dv:fl (fl- v2 v1))
         (define side-a-dw:fl (fl- w2 w1))
         (define side-a-dx:fxp (fx- x2:fxp x1:fxp))
         (define side-a-dl:fxp (fx- l2:fxp l1:fxp))
         (triangle-part y1 y2
                        x1:fxp x1:fxp side-a-dx:fxp side-a-dy
                        u1 v1 w1 l1:fxp
                        u1 v1 w1 l1:fxp
                        side-a-du:fl side-a-dv:fl side-a-dw:fl side-a-dl:fxp))
        (else
         (values x1:fxp u1 v1 w1 l1:fxp))))
    (define side-c-dy (fx- y3 y2))
    (when (fx> side-c-dy 0)
      (define side-c-du:fl (fl- u3 u2))
      (define side-c-dv:fl (fl- v3 v2))
      (define side-c-dw:fl (fl- w3 w2))
      (define side-c-dx:fxp (fx- x3:fxp x2:fxp))
      (define side-c-dl:fxp (fx- l3:fxp l2:fxp))
      (triangle-part y2 y3
                     x2:fxp side-b-x:fxp side-c-dx:fxp side-c-dy
                     u2 v2 w2 l2:fxp
                     side-b-u:fl side-b-v:fl side-b-w:fl side-b-l:fxp
                     side-c-du:fl side-c-dv:fl side-c-dw:fl side-c-dl:fxp)))

  ; Project lights
  (define lights (render-ctx-lights rctx))
  (for ((light lights))
    (define lvertex (rcge-light-vertex light))
    (mat4*vec4! (camera-vertex-camera lvertex)
                camera-matrix
                (world-vertex-world lvertex)))

  ; Start rendering at this point
  (define render-future
    (future
     (λ ()
       (start-hline-workers)
       )))

  ; scan-triangle all projected triangles
  (define borders (render-ctx-show-borders rctx))
  (define borders-list '())
  (pool-for-each
   (render-ctx-render-triangles rctx)
   (λ (tri)
     (define points (pipeline-triangle-vertices tri))
     (define Av (vector-ref points 0))
     (define Bv (vector-ref points 1))
     (define Cv (vector-ref points 2))
     (define A (pipeline-vertex-screen Av))
     (define B (pipeline-vertex-screen Bv))
     (define C (pipeline-vertex-screen Cv))
     (define x1 (fxvector-ref A 0))
     (define y1 (fxvector-ref A 1))
     (define x2 (fxvector-ref B 0))
     (define y2 (fxvector-ref B 1))
     (define x3 (fxvector-ref C 0))
     (define y3 (fxvector-ref C 1))
     (define Ap (pipeline-vertex-projection Av))
     (define Bp (pipeline-vertex-projection Bv))
     (define Cp (pipeline-vertex-projection Cv))
     (define w1 (flvector-ref Ap 3))
     (define w2 (flvector-ref Bp 3))
     (define w3 (flvector-ref Cp 3))
     (when borders
       (define lw1 (fl/ 1.01 w1))
       (define lw2 (fl/ 1.01 w2))
       (define lw3 (fl/ 1.01 w3))
       (set! borders-list (cons (list x1 y1 lw1 x2 y2 lw2 #xaa00aa)
                                borders-list))
       (set! borders-list (cons (list x1 y1 lw1 x3 y3 lw3 #xaa00aa)
                                borders-list))
       (set! borders-list (cons (list x3 y3 lw3 x2 y2 lw2 #xaa00aa)
                                borders-list)))
     (case (pipeline-triangle-type tri)
       ((flat flat/a)
        (scan-triangle-fill x1 y1 w1 x2 y2 w2 x3 y3 w3
                            (pipeline-triangle-surface tri)
                            (pipeline-triangle-alpha? tri)
                            ))
       ((lit lit/a)
        (define norm (pipeline-triangle-normal tri))
        (define Al (rcge-vertex-light Av norm lights))
        (define Bl (rcge-vertex-light Bv norm lights))
        (define Cl (rcge-vertex-light Cv norm lights))
        (define w1 (flvector-ref Ap 3))
        (define w2 (flvector-ref Bp 3))
        (define w3 (flvector-ref Cp 3))
        (scan-triangle-fill/l x1 y1 w1 Al x2 y2 w2 Bl x3 y3 w3 Cl
                              (pipeline-triangle-surface tri)
                              (pipeline-triangle-alpha? tri)
                              ))
       ((textured textured/a)
        (define texs (pipeline-triangle-uvcoords tri))
        (define TA (vector-ref texs 0)) 
        (define TB (vector-ref texs 1)) 
        (define TC (vector-ref texs 2))
        (define u1 (flvector-ref TA 0))
        (define v1 (flvector-ref TA 1))
        (define u2 (flvector-ref TB 0))
        (define v2 (flvector-ref TB 1))
        (define u3 (flvector-ref TC 0))
        (define v3 (flvector-ref TC 1))
        (scan-triangle-tex x1 y1 u1 v1 w1
                           x2 y2 u2 v2 w2
                           x3 y3 u3 v3 w3
                           (pipeline-triangle-surface tri)
                           (pipeline-triangle-alpha? tri)
                           ))
       ((texlit texlit/a)
        (define texs (pipeline-triangle-uvcoords tri))
        (define TA (vector-ref texs 0)) 
        (define TB (vector-ref texs 1)) 
        (define TC (vector-ref texs 2))
        (define norm (pipeline-triangle-normal tri))
        (define Al (rcge-vertex-light Av norm lights))
        (define Bl (rcge-vertex-light Bv norm lights))
        (define Cl (rcge-vertex-light Cv norm lights))
        (define u1 (flvector-ref TA 0))
        (define v1 (flvector-ref TA 1))
        (define u2 (flvector-ref TB 0))
        (define v2 (flvector-ref TB 1))
        (define u3 (flvector-ref TC 0))
        (define v3 (flvector-ref TC 1))
        (scan-triangle-tex/l x1 y1 u1 v1 w1 Al
                             x2 y2 u2 v2 w2 Bl
                             x3 y3 u3 v3 w3 Cl
                             (pipeline-triangle-surface tri)
                             (pipeline-triangle-alpha? tri)
                             )))
     ))

  (let loop ((i 0))
    (when (fx< i num-futures)
      (mfqueue-enqueue! hlines-queue #f)
      (loop (fx+ i 1))))
  
  (fsemaphore-post main-hline-worker-sema)
  (touch render-future)
  (pool-reset! (render-ctx-solid-triangles rctx))
  (pool-reset! (render-ctx-alpha-triangles rctx))
  
  (when borders
    (for ((border borders-list))
      (apply draw-line-w-buffer border)))
  #;(display (~a "\r"
                 (for/list ((cnt counters))
                   (~a #:width 5 #:align 'right cnt))
                 (~a #:width 5 #:align 'right (for/sum ((i num-futures)) (fxvector-ref counters i)))
                 " "
                 (~a #:width 5 #:align 'right hlines-pool-idx)
                 ))
  (void))
